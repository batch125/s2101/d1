
let express = require("express");

const PORT = 4000;

let app = express();
 
app.use(express.json());
app.use(express.urlencoded({extended:true}));


app.get("/", (req, res)=> res.send(`Hello World`));

//mini activity
  //create a route that will show a message "Hello from the /hello endpoint"

app.get("/hello", (req, res) => res.send(`Hello from the /hello Endpoint`));


//mini activity #2
  
app.post("/greeting", (req, res) => {
  // console.log(req.body);
  // console.log("Hello")
  res.send(`Hello there,${req.body.firstname}!`);

  // console.log(`Hello there, !`)
});



app.listen(PORT, ()=> {
  console.log(`Server running at port ${PORT}`)
});